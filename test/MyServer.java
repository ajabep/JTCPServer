package JTCPServer.test;

import JTCPServer.Server;
import JTCPServer.Service;

import java.io.IOException;
import java.net.InetAddress;

public class MyServer extends Server {

	public MyServer(int port) throws IOException {
		super(port);
	}

	public MyServer(int port, InetAddress ipAddress) throws IOException {
		super(port, ipAddress);
	}

	public MyServer(int port, String hostname) throws IOException {
		super(port, hostname);
	}

	@Override
	protected Service getNewInstanceOfService() {
		return new MyService();
	}
}
