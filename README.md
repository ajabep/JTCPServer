JTCPServer
=======

JTCPServer is an easy-to-use package in Java to create a server easily using the design pattern "Factory Method".

This package is created and tested with Java 8.


How use it
----------

Extends the `Service` and the `Server` abstract class.  
Next, run this code where you want to launch the server :

```java
JTPCServer.Server myServer = new MyServer(portToListen);
Thread myServerThread = new Thread(myServer);
myServerThread.start();
```

Just do it !


API
---

With your instance of the class `Server`, you have access to this API :

* `getInetAddress` to get the IP address where you are listening the port
* `getPort` to get the port you are listening


Tests
-----

It's create with JUnit4.
You can find them in the [test](test/) directory.


License
-------

JTCPServer is under GPL v2.0


