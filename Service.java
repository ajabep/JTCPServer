package JTCPServer;

import java.net.Socket;

public abstract class Service implements Runnable {

	/**
	 * the socket associated to this thread
	 */
	protected Socket socket;

	/**
	 * change the socket associated to this thread
	 * @param socket    the new associated socket
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	protected void finalize() throws Throwable {
		if (socket != null && !socket.isClosed())
			socket.close();

		super.finalize();
	}
}
