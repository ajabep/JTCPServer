package JTCPServer.test;

import JTCPServer.Service;

import static org.junit.Assert.*;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;





public class MyService extends Service {

	@Override
	public void run() {
		try {
			BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter socketOut = new PrintWriter(socket.getOutputStream(), true);

			String line = socketIn.readLine();



			socketOut.println(line);



			Thread.sleep(1L);
		}
		catch (InterruptedException e) {}
		catch (IOException e) {
			fail("IO Exception in the srv");
		}
	}
}
