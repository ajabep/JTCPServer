package JTCPServer.test;

import JTCPServer.Server;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServerTest {

	@Test
	public void test1_LaunchServer() throws Exception {
		int port = 9092;

		Server myServer = new MyServer(port++);
		Thread myServerThread = new Thread(myServer);
		myServerThread.start();
		Socket socket = new Socket(myServer.getInetAddress().getHostName(), myServer.getPort());

		assertTrue("Socket must be connected", socket.isConnected());

		socket.close();
		myServerThread.interrupt();


		String hostname = "localhost";
		myServer = new MyServer(port++, hostname);
		myServerThread = new Thread(myServer);
		myServerThread.start();
		socket = new Socket(myServer.getInetAddress().getHostName(), myServer.getPort());

		assertEquals("The server hostname must be the hostname we wanted.", hostname, myServer.getInetAddress().getHostName());
		assertTrue("Socket must be connected", socket.isConnected());

		socket.close();
		myServerThread.interrupt();


		String address = "127.0.0.1";
		myServer = new MyServer(port, address);
		myServerThread = new Thread(myServer);
		myServerThread.start();
		socket = new Socket(myServer.getInetAddress().getHostName(), myServer.getPort());

		assertEquals("The server address must be the address we wanted.", address, myServer.getInetAddress().getHostAddress());
		assertTrue("Socket must be connected", socket.isConnected());

		socket.close();
		myServerThread.interrupt();


	}

	@Test
	public void test1_Running() throws Exception {
		Server myServer = new MyServer(9090);
		Thread myServerThread = new Thread(myServer);
		myServerThread.start();
		Socket socket = new Socket(myServer.getInetAddress().getHostName(), myServer.getPort());

		assertTrue("Socket must be connected", socket.isConnected());

		socket.close();

		assertTrue("Socket must be closed", socket.isClosed());

		myServerThread.interrupt();
	}

	@Test(timeout = 1000L) // = timeout at 1 sec
	public void test2_IO() throws Exception {
		Server myServer = new MyServer(9091);
		Thread myServerThread = new Thread(myServer);
		myServerThread.start();
		Socket socket = new Socket(myServer.getInetAddress().getHostName(), myServer.getPort());
		BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter socketOut = new PrintWriter(socket.getOutputStream(), true);

		String strToPrint = "test I/O of JTCPServer";

		socketOut.println(strToPrint);
		socketOut.flush();

		assertEquals("The server must write what this thread had written", strToPrint, socketIn.readLine());

		socket.close();
		myServerThread.interrupt();
	}
}


