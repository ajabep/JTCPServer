package JTCPServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class Server implements Runnable{

	/**
	 * the server's socket used by this server
	 */
	private ServerSocket socket;

	/**
	 * Construct a server and listen a specific port.
	 * @param port  the port to listening
	 * @throws IOException
	 * @throws SecurityException
	 */
	public Server(int port) throws IOException, SecurityException  {
		this.socket = new ServerSocket(port);
	}

	/**
	 * Construct a server on a specific address and listen a specific port.
	 * @param port      the port to listen
	 * @param ipAddress the ip address at which to start the server
	 * @throws IOException
	 * @throws SecurityException
	 */
	public Server(int port, InetAddress ipAddress) throws IOException, SecurityException {
		this.socket = new ServerSocket();
		this.socket.bind(new InetSocketAddress(ipAddress, port));
	}

	/**
	 * Construct a server on a specific hostname and listen a specific port.
	 * @param port      the port to listen
	 * @param hostname  the hostname at which to start the server
	 * @throws IOException
	 * @throws SecurityException
	 */
	public Server(int port, String hostname) throws IOException, SecurityException {
		this.socket = new ServerSocket();
		this.socket.bind(new InetSocketAddress(hostname, port));
	}

	/**
	 * Function used when starting the server
	 */
	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()) {
			Socket socketToDeal = null;

			try {
				socketToDeal = this.socket.accept();
			} catch (IOException e) {
				synchronized (System.out) {
					System.err.println("Server's error - I/O Error");
					e.printStackTrace();
				}
				continue;
			}

			Service service = this.getNewInstanceOfService();

			service.setSocket(socketToDeal);
			new Thread(service).start();
		}
	}

	/**
	 * @see ServerSocket.getInetAddress
	 */
	public InetAddress getInetAddress() {
		return this.socket.getInetAddress();
	}

	/**
	 * @see ServerSocket.getLocalPort
	 */
	public int getPort() {
		return this.socket.getLocalPort();
	}

	/**
	 * create a new instance of the service.
	 * @return the instance of the service created.
	 */
	protected abstract Service getNewInstanceOfService();
}
